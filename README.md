XN
==

A extensible command-line client for XNAT

Features
--------

- Command-line tools
    * `request`: Make an http requests to the XNAT REST API
    * `download`: Recursively download file resources from XNAT projects,
      subjects or imaging sessions

- Extensible Python interface
    * Based on the excellent [click](https://click.palletsprojects.com) library
      by The Pallets Projects.

- Designed to support multiple XNAT client library implementations
    * [protocol.py](xn/protocol.py) describes the interface that client library providers should expose
    * Currently only supports [xnatpy](https://xnat.readthedocs.io), see
      [providers/xnatpy.py](xn/providers/xnatpy.py)


Install
-------

```sh
pip install git+https://gitlab.gwdg.de/medinfpub/mi-xnat/xnat/clients/xn.git
```

Command line usage
------------------

```sh
export XNAT_HOST="https://xnat.miskatonic.edu"
export XNAT_USER="xxxxxxxx-xxxx-xxxx-xxxxxxxxxxxxxxxxx" # token alias 
export XNAT_PASS="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" # token secret

xn --help
xn request --help
xn download --help
```


Extending
---------

- Create a new python file and import click and XN

```py
import click, xn
```

- Extend the root command, XN will provide a managed HTTP session in the context

```py
@click.command()
@click.pass_context
def command_with_session(ctx, *args, **kwargs):
    session = ctx.obj
    ...

xn.cli.add_command(command_with_session)
```

- Extend the request command, XN will provide a http response object in the
  context

```py
@click.command()
@click.pass_context
def command_with_response(ctx, *args, **kwargs):
    response = ctx.obj
    ...

xn.request.add_command(pretty_print)
```   

- Call `cli()` in your script's `__main__`

```py
if __name__ == '__main__':
    xn.cli()
```

- Run your script

```sh
chmod +x <script>.py
./<script>.py --help
./<script>.py command_with_session --help
./<script>.py request <path> command_with_response --help
```

- See [examples/extending_commands.py](examples/extending_commands.py) for a
  more complete example

TODO
----

- Unit testing
- Implement simple [requests](https://docs.python-requests.org) provider for
  testing, debugging and benchmarking
- Implement [pyxnat](https://pyxnat.github.io) provider
- Investigate run time client library generation
  for [/xapi endpoints](https://www.xnat.org/api/xnat-180.json). (using eg,
  [Bravado](https://github.com/Yelp/bravado))