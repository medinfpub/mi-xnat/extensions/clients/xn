#!/usr/bin/env python3
#
# USAGE:
# ./extending_commands.py healthcheck
# ./extending_commands.py request --format=csv /data/projects pretty-print
# ./extending_commands.py request --format=json /data/projects pretty-print


import click

from xn import cli, request


# Extend the root command, XN will provide a managed HTTP session in the
# context

@click.command()
@click.pass_context
def healthcheck(ctx, *args, **kwargs):
    session = ctx.obj
    response  = session.head('/')
    click.echo(response.status_code)


cli.add_command(healthcheck)


# Extend the request command, XN will provide a http response object in the
# context

import json

@click.command()
@click.pass_context
def pretty_print(ctx, *args, **kwargs):
    response = ctx.obj

    if response.headers['Content-Type'] == 'text/csv':
        for l in response.text.split('\n'):
            click.echo(l.replace(',', '\t'))

    elif response.headers['Content-Type'] == 'application/json':
        click.echo(json.dumps(json.loads(response.text), indent=2))
        
        
request.add_command(pretty_print)

cli.add_command(request)


if __name__ == '__main__':
    cli()
