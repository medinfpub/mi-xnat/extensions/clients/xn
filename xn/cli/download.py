import click


@click.group(name='download', invoke_without_command=True)
@click.argument('path', type=str)
@click.argument('target-dir', type=str)
@click.pass_context
def download(ctx, path, target_dir, **kwargs):
    session = ctx.obj
    session.download_dir(path, target_dir)
