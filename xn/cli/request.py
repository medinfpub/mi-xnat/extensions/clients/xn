import click

from .utils import zip_to_dict, read_file, echo


@click.group(name='request', invoke_without_command=True)
@click.argument('path', type=str)
@click.option('--method', type=str, default='GET')
@click.option('--path', type=str)
@click.option('--format', type=click.Choice(['json', 'csv', 'raw', 'human', 'zip'], case_sensitive=False), default='json')
@click.option('--query', type=(str,str), multiple=True, callback=zip_to_dict)
@click.option('--data', type=click.File('rb'), callback=read_file)
@click.option('--headers', type=(str,str), multiple=True, callback=zip_to_dict)
@click.pass_context
def request(ctx, method=None, **kwargs):
    session = ctx.obj

    method_switch = {
        'head': session.head,
        'get': session.get,
        'post': session.post,
        'put': session.put,
        'delete': session.delete,
    }

    func = method_switch[method.lower()]

    ctx.obj = ctx.with_resource(func(**kwargs))

    @ctx.call_on_close
    def echo_without_command():
        if ctx.invoked_subcommand:
            return
        echo(ctx.obj.text)
