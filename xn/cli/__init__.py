import click

from ..providers import default_session as Session

from .request import request
from .download import download


@click.group()
@click.option('--host', type=str, envvar='XNAT_HOST')
@click.option('--user', type=str, envvar='XNAT_USER')
@click.option('--password', type=str, envvar='XNAT_PASS', hidden=True)
@click.option('--default-timeout', type=int, envvar='XNAT_TIMEOUT')
@click.option('--auth-provider', type=str, envvar='XNAT_AUTH_PROVIDER')
@click.option('--jsession', type=str, envvar='XNAT_JSESSION')
@click.pass_context
def cli(ctx, **kwargs):
    session = Session(**kwargs)
    ctx.obj = ctx.with_resource(session)


@click.command()
@click.pass_context
def debug_command(ctx):
    import pdb ; pdb.set_trace()


request.add_command(debug_command)
download.add_command(debug_command)

cli.add_command(request)
cli.add_command(download)
cli.add_command(debug_command)
