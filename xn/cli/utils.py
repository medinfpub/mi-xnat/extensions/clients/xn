import click


def zip_to_dict(ctx, param, value):
    if value is not None:
        return dict(value)


def read_file(ctx, param, value):
    if value is not None:
        return value.read()


def warn(message, **kwargs):
    click.echo(click.style('WARNING ', fg='bright_red') + message, err=True, **kwargs)


def echo(message, **kwargs):
    click.echo(message, **kwargs)