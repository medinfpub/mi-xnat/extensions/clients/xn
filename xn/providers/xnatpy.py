import xnat

from types import MethodType
from pathlib import Path
from contextlib import contextmanager

from .base import AbstractBaseSession


@contextmanager
def _patch_method(inst, attr, f):
    """Context management utility to patch instance methods

    Replaces the given instance attribute with a new function and restores it
    on exit
    """
    fo = getattr(inst, attr)
    try:
        setattr(inst, attr, MethodType(f, inst))
        yield inst
    finally:
        setattr(inst, attr, fo)


def _create_object(self, uri, **kwargs):
    """Monkey for patch  `create_object`

    XNATpy's `BaseXNATSession.create_object` crashes if provided with the uri
    of an image session with a custom data type, regardless of the
    `extension_types` connection parameter. So we force it to return a generic
    image session object instead. Note that this workaround is only possible if
    `extension_types` is disabled
    """
    f = xnat.session.BaseXNATSession.create_object
    path = Path(uri)
    if path.match('**/projects/**/subjects/**/experiments/*'):
        kwargs['type_'] = 'xnat:imageSessionData'
    return f(self, uri, **kwargs)


class XNATPYSession(AbstractBaseSession):
    _defaults = {
        'user': None,
        'password': None,
        'default_timeout': 300,
        'auth_provider': None,
        'jsession': None,
        'verify': True,
        'netrc_file': None,
        'debug': False,
        'extension_types': False,
        'loglevel': None,
        'logger': None,
        'detect_redirect': True,
        'no_parse_model': False,
        'cli': True
    }
    
    def __init__(self, host, **kwargs):
        _options = { 'server': host, **self._defaults, **kwargs }

        self.host = host
        self._delegate = xnat.connect(**_options)

    def head(self, path, headers=None, **kwargs):
        return self._delegate.head(path, headers=None)

    def get(self, path, format=None, query=None, headers=None, **kwargs):
        return self._delegate.get(path, format=format, query=query, headers=None)

    def post(self, path, format=None, query=None, data=None, headers=None, **kwargs):
        return self._delegate.post(path, format=format, query=query, data=data, headers=None)

    def put(self, path, format=None, query=None, data=None, headers=None, **kwargs):
        return self._delegate.put(path, format=format, query=query, data=data, headers=None)

    def delete(self, path, query=None, headers=None, **kwargs):
        return self._delegate.delete(path, query=query, headers=None)

    def download_dir(self, path, target_dir):
        with _patch_method(self._delegate, 'create_object', _create_object):
            obj = self._delegate.create_object(path)
            obj.download_dir(target_dir)
    
    def close(self):
        self._delegate.disconnect()


class XNATPYMiniSession(XNATPYSession):
    _defaults = {
        **XNATPYSession._defaults,
        'no_parse_model': True
    }

    def download_dir(self, path, target_dir):
        self._not_impl_err(self.download_dir)