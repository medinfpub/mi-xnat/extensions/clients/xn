from contextlib import AbstractContextManager

from ..protocol import ISession

class AbstractBaseSession(AbstractContextManager, ISession): 
    def _not_impl_err(self, f):
        msg = type(self).__name__ + ' does not support ' + f.__name__
        raise NotImplementedError(msg)

    def __exit__(self, exc_type=None, exc_value=None, traceback=None):
        return self.close()