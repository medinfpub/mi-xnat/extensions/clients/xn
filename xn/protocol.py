from abc import abstractmethod
from types import TracebackType
from typing import TypeVar, Protocol, Any, Text, Mapping, Iterator, Literal, TypedDict
from typing_extensions import Unpack


_T_co = TypeVar('_T_co', covariant=True)


class ISessionParams(TypedDict):
    host: str | None
    user: str | None
    password: str | None
    default_timeout: str | None
    auth_provider: str | None
    jsession: str | None


class IRequestParams(TypedDict):
    format: Literal['json', 'csv', 'raw', 'human', 'zip'] | None
    query: Mapping[str, str] | None
    data: bytes | None
    headers: Mapping[str, str] | None
    stream: bool | None


class IResponse(Protocol):
    status_code: int
    text: Text
    content: bytes
    reason: Text
    headers: Mapping[str, str]

    @abstractmethod
    def iter_lines(self) -> Iterator[Any]:
        ...

    @abstractmethod
    def iter_content(self) -> Iterator[Any]:
        ...


class ISession(Protocol[_T_co]):

    @abstractmethod
    def __init__(self, **kwargs: Unpack[ISessionParams]) -> None:
        ...
    
    @abstractmethod
    def head(self, path: str, **kwargs: Unpack[IRequestParams]) -> IResponse:
        ...

    @abstractmethod
    def get(self, path: str, **kwargs: Unpack[IRequestParams]) -> IResponse:
        ...

    @abstractmethod
    def post(self, path: str, **kwargs: Unpack[IRequestParams]) -> IResponse:
        ...

    @abstractmethod
    def put(self, path: str, **kwargs: Unpack[IRequestParams]) -> IResponse:
        ...

    @abstractmethod
    def delete(self, path: str, **kwargs: Unpack[IRequestParams]) -> IResponse:
        ...

    @abstractmethod
    def download_dir( elf, path: str, target_dir: str,) -> None:
        ...

    @abstractmethod
    def close(self) -> None:
        ...

    # Context Manager Protocol

    @abstractmethod
    def __enter__(self) -> _T_co:
        ...

    @abstractmethod
    def __exit__(self, __exc_type: type[BaseException] | None, __exc_value: BaseException | None, __traceback: TracebackType | None,) -> bool | None:
        ...