from setuptools import setup, find_packages

setup(
    name='xn',
    version='0.1.0-dev',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'click==8.1.3',
        'xnat==0.5.1'
    ],
    entry_points={
        'console_scripts': [
            'xn = xn:cli',
        ],
    },
)
